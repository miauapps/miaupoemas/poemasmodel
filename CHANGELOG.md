- 1812.1-Beta
    - Se agrega clase de prueba para testear jitpack

- 1812.2-RC
    - Se agregan clases modelo necesarias para el proyecto
    - Se agrega funcionalidad BytesParser para el web service
    
- 1812.3-RC
    - Se agrega campo readed a viewPresentMenu
    
- 1812.4-RC
    - Se agrega campo readed a present

- 1905.1-RC
    - Arquitectura modificada para hibernate

- 1905.2-RC
    - Se agrega clase App y se definen lenghts
    a los String
    
- 1905.2.1-RC
    - Correcciones menores en clases Entity
    
- 1905.2.2-RC
    - Correcciones menores en clase Present

- 1908.1-RC
	- Correcciones menores
