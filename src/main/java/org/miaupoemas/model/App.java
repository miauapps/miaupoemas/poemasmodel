package org.miaupoemas.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity(name = "app")
public class App {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Integer id;

    @Column(length = 10)
    private String version;

    public App() {}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
