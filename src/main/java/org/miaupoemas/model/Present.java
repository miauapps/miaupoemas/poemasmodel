package org.miaupoemas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Calendar;

@Entity(name = "present")
public class Present {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "native")
    @GenericGenerator(name="native", strategy = "native")
    private Integer id;

    // present no puede coexistir sin presentType => manyToOne
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name="idType")
    //@JsonIgnore
    private PresentType type;

    @Column(columnDefinition = "blob")
    private byte[] icon;

    @Column
    @Temporal(value = TemporalType.TIMESTAMP)
    @CreatedDate
    private Calendar datetime;

    @Column(nullable = false)
    private boolean readed;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public PresentType getType() {
        return type;
    }

    public void setType(PresentType type) {
        this.type = type;
    }

    public byte[] getIcon() {
        return icon;
    }

    public void setIcon(byte[] icon) {
        this.icon = icon;
    }

    public Calendar getDatetime() {
        return datetime;
    }

    public void setDatetime(Calendar datetime) {
        this.datetime = datetime;
    }

    public boolean isReaded() {
        return readed;
    }

    public void setReaded(boolean readed) {
        this.readed = readed;
    }
}
