package org.miaupoemas.model.tools;

public class BytesParser {
    public static byte[] toByteArray(String data) {
        String[] dataSplit = data.split(",");
        byte[] array = new byte[dataSplit.length];
        for (int i = 0; i < dataSplit.length; i++)
            array[i] = Byte.parseByte(dataSplit[i]);
        return array;
    }

    public static String toString(byte[] data) {
        StringBuilder sbBytes = new StringBuilder();
        for (int i = 0; i < data.length; i++)
            sbBytes.append(data[i]).append(',');
        sbBytes.deleteCharAt(sbBytes.length()-1);
        return sbBytes.toString();
    }
}
