package org.miaupoemas.model;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;

@Entity(name = "presentType")
public class PresentType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "native")
    @GenericGenerator(name="native", strategy = "native")
    private Integer id;

    @Column(length = 40)
    private String name;

    //@OneToMany(targetEntity = Present.class, mappedBy = "type",
    //        cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    //private List<Present> presents;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /*public List<Present> getPresents() {
        return presents;
    }

    public void setPresents(List<Present> presents) {
        this.presents = presents;
    }*/
}
